﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreBetterer.DataStructurePractice.DataStructures
{
    public class BTNode
    {
        public BTNode()
        {

        }


        public BTNode(int f)
        {
            data = f;
        }

        public int data;
        public BTNode LChild;
        public BTNode RChild;
    }

    public class BinaryTree
    {

        public BTNode _root;

        public BinaryTree()
        {
            _root = null;
            _orderresults = "";
        }


        public bool InsertNode(int f)
        {
            BTNode toinsert = new BTNode();
            toinsert.data = f;

            if (_root == null)
                _root = toinsert;
            else
            {
                BTNode current = _root;
                BTNode parent;
                while (true)
                {
                    parent = current;
                    if (f < current.data)
                    {
                        current = current.LChild;
                        if (current == null)
                        {
                            parent.LChild = toinsert;
                            return true;
                        }

                    }
                    else
                    {
                        current = current.RChild;
                        if (current == null)
                        {
                            parent.RChild = toinsert;
                            return true;
                        }

                    }

                }

            }




            return false;
        }

        private string _orderresults;

        public string Preorder()
        {
            string results = "";
            Preorder(_root);
            results = _orderresults;
            _orderresults = "";
            return results;
        }

        public void Preorder(BTNode current)
        {

            if (current != null)
            {
                _orderresults = string.Concat(_orderresults, current.data);
                Preorder(current.LChild);
                Preorder(current.RChild);

            }
            return;
        }



        public string Inorder()
        {
            string results = "";
            Inorder(_root);
            results = _orderresults;
            _orderresults = "";
            return results;
        }

        public void Inorder(BTNode current)
        {

            if (current != null)
            {
                Inorder(current.LChild);
                _orderresults = string.Concat(_orderresults, current.data);
                Inorder(current.RChild);

            }
            return;
        }


        public string Postorder()
        {
            string results = "";
            Postorder(_root);
            results = _orderresults;
            _orderresults = "";
            return results;
        }

        public void Postorder(BTNode current)
        {

            if (current != null)
            {
                Postorder(current.LChild);
                Postorder(current.RChild);
                _orderresults = string.Concat(_orderresults, current.data);

            }
            return;
        }




    }
}