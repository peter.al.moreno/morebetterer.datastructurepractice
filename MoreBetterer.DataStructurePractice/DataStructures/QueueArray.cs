﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreBetterer.DataStructurePractice.DataStructures
{
    public class QueueArray
    {
        private char[] _array;
        private int _tail;
        private int _head;


        public QueueArray()
        {
            _array = new char[5];
            //1 less that max length
            _tail = _array.Length - 1;
            _head = _array.Length - 1;
        }


        public QueueArray(int size)
        {
            _array = new char[size];
            //1 less that max length
            _tail = _array.Length - 1;
            _head = _array.Length - 1;
        }



        public string StringOfArray()

        {

            string output = "";
            foreach (char f in _array)
            {
                output += f;
            }

            return output;
        }



        public int getIndexTail()
        { return _tail; }

        public int getIndexHead()
        { return _head; }


        public bool enqueue(char f)
        {
            if (_tail == 0)
                growQueue2x();

            try
            {
                _array[_tail] = f;
                _tail--;

            }
            catch
            {
                return false;
            }

            return true;


        }
        public char peek()
        {
            return _array[_head];

        }

        public bool dequeue()
        {

            try
            {

                _head--;
            }
            catch
            {
                return false;
            }





            return true;


        }



        public void empty()
        {
            _head = _tail;
        }


        private void growQueue2x()
        {

            char[] tempArray = new char[_array.Length * 2];

            //            Array.Copy(_array, 0, tempArray, 0, _array.Length);
            int temptail = (tempArray.Length) - 1;
            for (int i = _array.Length; i > 0; i--)
            {

                if (i <= _head)//only copy what is still in scope, ignoring "removed" heads
                {
                    tempArray[temptail] = _array[i];
                    temptail--;
                }
            }


            _tail = temptail; //array is set to the temptail
            _head = tempArray.Length - 1; //header will always be the end of the new array
            _array = tempArray; //
        }


    }
}