﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoreBetterer.DataStructurePractice.DataStructures
{
    public class StackLinkedList
    {

        //single linked list has a head (top)
        //pros , 1- no hard size depth limit, 2 - easy to implement (no bounds checking and emptylist = empty stack)
        //cons memory allocation on push. Per-node memory overhead



        private Node _Top = new Node();


        private class Node
        {
            public Node next;
            public char data;
        }




        public bool push(char F)
        {
            try
            {
                Node toAdd = new Node(); //create node
                toAdd.data = F;  // assign data
                toAdd.next = _Top; //new node now points at current head
                _Top = toAdd; //head is now the new node. old node still exists as pointed by .next
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;

            }
            return true;
        }


        public bool pop()
        {
            if (_Top == null)
                return false;

            try
            {
                _Top = _Top.next;
            }
            catch
            {

                return false;
            }

            return true;
        }


        public char top()
        {
            return _Top.data;

        }















    }
}