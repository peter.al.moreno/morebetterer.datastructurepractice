﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoreBetterer.DataStructurePractice.DataStructures;

namespace MoreBetterer.DataStructurePractice.Tests.DataStructures
{
    [TestClass()]
    public class BinaryTreeTests
    {
        [TestMethod()]
        public void BinaryTreeTest()
        {
            BinaryTree TBTree = new BinaryTree();

            return;

        }

        [TestMethod()]
        public void InsertNode()
        {
            BinaryTree TBTree = new BinaryTree();
            TBTree.InsertNode(5);
            TBTree.InsertNode(2);
            TBTree.InsertNode(8);
            TBTree.InsertNode(1);
            TBTree.InsertNode(4);
            TBTree.InsertNode(6);
            TBTree.InsertNode(9);


            /*
*                     5
*                 /       \
*                2         8
*               / \       / \
*              1   4     6   9
* 
*/


            string results = TBTree.Preorder();

            bool comparison = false;
            if (results == "5214869")
                comparison = true;


            Assert.IsTrue(comparison);
            return;

        }
        [TestMethod()]
        public void FindNode()
        {
            BinaryTree TBTree = new BinaryTree();

            return;

        }

        [TestMethod()]
        public void RemoveNode()
        {
            BinaryTree TBTree = new BinaryTree();

            return;

        }







        /*
        *                  1
        *                 / \
        *                2   3
        *               / \
        *              4   5
        * 
        */

        [TestMethod()]
        public void TraversalOrderPre()
        {
            BinaryTree TBTree = new BinaryTree();
            TBTree._root = new BTNode(1);
            TBTree._root.LChild = new BTNode(2);
            TBTree._root.RChild = new BTNode(3);
            TBTree._root.LChild.LChild = new BTNode(4);
            TBTree._root.LChild.RChild = new BTNode(5);



            string results = TBTree.Preorder();

            bool comparison = false;
            if (results == "12453")
                comparison = true;

            Assert.IsTrue(comparison);

        }






        [TestMethod()]
        public void TraversalOrderIn()
        {
            BinaryTree TBTree = new BinaryTree();
            TBTree._root = new BTNode(1);
            TBTree._root.LChild = new BTNode(2);
            TBTree._root.RChild = new BTNode(3);
            TBTree._root.LChild.LChild = new BTNode(4);
            TBTree._root.LChild.RChild = new BTNode(5);



            string results = TBTree.Inorder();

            bool comparison = false;
            if (results == "42513")
                comparison = true;

            Assert.IsTrue(comparison);

            return;

        }


        [TestMethod()]
        public void TraversalOrderPost()
        {
            BinaryTree TBTree = new BinaryTree();
            TBTree._root = new BTNode(1);
            TBTree._root.LChild = new BTNode(2);
            TBTree._root.RChild = new BTNode(3);
            TBTree._root.LChild.LChild = new BTNode(4);
            TBTree._root.LChild.RChild = new BTNode(5);



            string results = TBTree.Postorder();

            bool comparison = false;
            if (results == "45231")
                comparison = true;

            Assert.IsTrue(comparison);

            return;

        }


    }
}
