﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoreBetterer.DataStructurePractice.DataStructures;

namespace MoreBetterer.DataStructurePractice.Tests.DataStructures
{
    [TestClass()]
    public class QueueArrayTests
    {
        [TestMethod()]
        public void QueueArrayTest()
        {
            QueueArray Tarray = new QueueArray();
            return;
        }



        [TestMethod()]
        public void enqueue()
        {
            QueueArray Tarry = new QueueArray();
            bool didenqueue = Tarry.enqueue('A');
            Assert.IsTrue(didenqueue);
        }

        [TestMethod()]
        public void dequeue()
        {
            QueueArray Tarry = new QueueArray();
            bool diddequeue = Tarry.dequeue();
            Assert.IsTrue(diddequeue);
        }


        [TestMethod]
        public void peek()
        {
            QueueArray Tarry = new QueueArray();
            Tarry.enqueue('A');
            var peekvalue1 = Tarry.peek();
            Tarry.enqueue('B');
            var peekvalue2 = Tarry.peek();
            Tarry.enqueue('C');
            string values1 = Tarry.StringOfArray();
            Tarry.enqueue('D');
            Tarry.enqueue('E');
            string values2 = Tarry.StringOfArray();
            Tarry.enqueue('F');
            Tarry.enqueue('G');
            Tarry.enqueue('H');
            Tarry.dequeue();
            Tarry.dequeue();
            Tarry.dequeue();
            Tarry.dequeue();
            Tarry.dequeue();
            Tarry.dequeue();
            var peekvalue3 = Tarry.peek();
            bool matching = false;

            string values3 = Tarry.StringOfArray();

            Tarry.enqueue('a');
            Tarry.enqueue('b');
            Tarry.enqueue('c');
            Tarry.enqueue('d');
            Tarry.enqueue('e');
            Tarry.enqueue('f');
            Tarry.enqueue('g');
            Tarry.enqueue('h');
            Tarry.enqueue('i');
            Tarry.enqueue('j');
            Tarry.enqueue('k');
            Tarry.enqueue('l');
            Tarry.enqueue('m');
            Tarry.enqueue('n');
            Tarry.enqueue('o');
            Tarry.dequeue();
            Tarry.dequeue();
            var peekvalue4 = Tarry.peek();
            string values4 = Tarry.StringOfArray();


            if (peekvalue1 == 'A' && peekvalue2 == 'A' && peekvalue3 == 'G' && peekvalue4 == 'a')
                matching = true;

            Assert.IsTrue(matching);
        }



        [TestMethod]
        public void DequeueAll()
        {
            QueueArray Tarry = new QueueArray();
            Tarry.enqueue('a');
            Tarry.enqueue('b');
            Tarry.enqueue('c');
            Tarry.enqueue('d');
            Tarry.enqueue('e');
            Tarry.enqueue('f');
            Tarry.enqueue('g');
            Tarry.enqueue('h');
            Tarry.enqueue('i');
            Tarry.enqueue('j');
            Tarry.enqueue('k');
            Tarry.enqueue('l');
            Tarry.enqueue('m');
            Tarry.enqueue('n');
            Tarry.enqueue('o');
            Tarry.empty();

            char test = Tarry.peek();

            Assert.AreEqual('\0', test);

        }



        [TestMethod]
        public void CheckHeadTailPositions()
        {
            QueueArray Tarry = new QueueArray();
            int[] PosTests = new int[10];
            PosTests[0] = Tarry.getIndexHead(); //should be 4
            PosTests[1] = Tarry.getIndexTail();//should be 4
            Tarry.enqueue('A');
            Tarry.enqueue('B');
            Tarry.enqueue('C');
            Tarry.enqueue('D');
            PosTests[2] = Tarry.getIndexHead(); //should be 4
            PosTests[3] = Tarry.getIndexTail();//should be 0
            Tarry.enqueue('E');
            PosTests[4] = Tarry.getIndexHead(); //should be 9
            PosTests[5] = Tarry.getIndexTail();//should be 4
            string values1 = Tarry.StringOfArray();
            Tarry.dequeue();
            Tarry.dequeue();
            Tarry.dequeue();
            PosTests[6] = Tarry.getIndexHead(); //should be 6
            PosTests[7] = Tarry.getIndexTail();//should be 4
            Tarry.enqueue('a');
            Tarry.enqueue('b');
            Tarry.enqueue('c');
            Tarry.enqueue('d');
            Tarry.enqueue('e');
            PosTests[8] = Tarry.getIndexHead(); //should be 19
            PosTests[9] = Tarry.getIndexTail();//should be 12
            string values2 = Tarry.StringOfArray();


            int[] rightanswers = { 4, 4, 4, 0, 9, 4, 6, 4, 19, 12 };
            bool doesmatch = false;
            if (PosTests.SequenceEqual(rightanswers))
            {
                doesmatch = true;
            }
            Assert.IsTrue(doesmatch);

        }



    }
}
