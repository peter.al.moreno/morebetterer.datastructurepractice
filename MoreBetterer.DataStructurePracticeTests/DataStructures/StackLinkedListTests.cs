﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoreBetterer.DataStructurePractice.DataStructures;

namespace MoreBetterer.DataStructurePractice.Tests.DataStructures
{
    [TestClass()]
    public class StackLinkedListTests
    {
        [TestMethod()]
        public void StackLinkedListTest()
        {
            StackLinkedList TStackList = new StackLinkedList();
            return;
        }


        [TestMethod()]
        public void GetTop()
        {
            StackLinkedList TStackList = new StackLinkedList();

            TStackList.push('a');

            char TEST = TStackList.top();

            Assert.AreEqual('a', TEST);


        }

        [TestMethod()]
        public void push()
        {

            //int[] results = new int[4];
            StackLinkedList TStackList = new StackLinkedList();
            Assert.IsTrue(TStackList.push('a'));


        }

        [TestMethod()]
        public void pop()
        {
            StackLinkedList TStackList = new StackLinkedList();
            Assert.IsTrue(TStackList.pop());

        }


        [TestMethod()]
        public void ComplicatedTop()
        {
            char[] ExpectedResults = { 'a', 'e', 'B', 'B' };

            char[] TestResults = new char[4];
            StackLinkedList TStackList = new StackLinkedList();
            TStackList.push('a');
            TStackList.push('b');
            TStackList.push('c');
            TStackList.pop();
            TStackList.pop();
            TestResults[0] = TStackList.top();

            TStackList.push('d');
            TStackList.push('e');
            TestResults[1] = TStackList.top();

            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.push('0');
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();
            TStackList.pop();


            TStackList.push('A');
            TStackList.push('B');

            TestResults[2] = TStackList.top();
            TestResults[3] = TStackList.top();


            Assert.IsTrue(ExpectedResults.SequenceEqual(TestResults));

        }




    }
}
